var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({extended: false})


app.get('/', function (req, res) {
   res.sendFile( __dirname + "/" + "body.html" );
   
})

app.post('/login.html', urlencodedParser, function (req, res) {
   res.sendFile( __dirname + "/" + "login.html" );
   response = {
      Name:req.body.name1,
      Password:req.body.passw
   };
   console.log(response);
   
})

var server = app.listen(8081, function () {
   var host = server.address().address;
   var port = server.address().port;
   
   console.log("Server is running at ", host, port);

})